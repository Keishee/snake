package snake;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author da-sil_l
 */
public class Snake extends Application {

    private HashMap<Body, String> snake;
    private ArrayList<Others> obstacles;
    private GraphicsContext snakeGC;
    private Group root;
    private String nextDirection;
    private String lastDirection;
    private Others powerUp;
    private final static Instru instru = new Instru();
    public static final int WIDTH = 300;
    public static final int HEIGHT = 250;
    private static int WAIT_TIME;
    private static final int INITIAL_WAIT_TIME = 300;
    public static final int TRANSFORM = 2;
    private static int score = 0;
    private Scene scene;
    private Scene scoreScene;
    private Stage primaryStage;
    private Timeline timeline;
    private boolean STOP = false;
    private boolean canUpdate = true;
    private PerspectiveCamera camera;
    private Canvas snakeFrame;

    /**
     * displays the snake body and the PowerUp
     */
    private void displaySnake() {
        snakeGC.setFill(Color.BLACK);
        snakeGC.fillRect(0, 0, WIDTH * 10, HEIGHT * 10);
        Set<Body> keySet = snake.keySet();
        root.getChildren().clear();
        root.getChildren().add(snakeFrame);
        root.getChildren().add(camera);
        if (powerUp != null) {
            root.getChildren().add(powerUp);
        }
        for (Others obstacle : obstacles) {
            root.getChildren().add(obstacle);
        }
        for (Body body : keySet) {
            root.getChildren().add(body);
        }
    }

    /**
     * set the direction of each body in the snake if the direction isnt
     * changed, the direction is set to itself so body can update his position
     *
     * @param turn which body should be updated (POSSIBLE REASON WHY MULTI
     * DIRECTION DOESNT WORK)
     */
    private void computeSnakeDirection() {
        Set keys = snake.keySet();
        Object[] bodyTab = keys.toArray();
        String save = getBody(0).getDirection();
        int checked = 0;

        while (checked != snake.size()) {
            for (int i = 0; i < bodyTab.length; ++i) {
                if (((Body) bodyTab[i]).getOrder() == checked) {
                    if (checked == 0) {
                        save = getBody(0).getDirection();
                        snake.replace(getBody(checked), nextDirection);
                        getBody(0).setDirection(nextDirection);
                    } else {
                        String tmp = getBody(checked).getDirection();
                        snake.replace(getBody(checked), save);
                        getBody(checked).setDirection(save);
                        save = tmp;
                    }
                    ++checked;
                    break;
                }
            }
        }
    }

    /**
     * massive pile of shit, for each Body in the snake, looks at his direction
     * and update the "pos" table
     *
     * @return
     */
    private int[] computeNewBodyPos() {
        int[] pos = new int[2];
        Set<Body> keys = snake.keySet();
        pos[0] = getBody(0).getPosX();
        pos[1] = getBody(0).getPosY();
        for (Body body : keys) {
            switch (body.getDirection()) {
                case "right":
                    pos[0] = pos[0] - Body.getSIZE();
                    break;
                case "left":
                    pos[0] = pos[0] + Body.getSIZE();
                    break;
                case "up":
                    pos[1] = pos[1] + Body.getSIZE();
                    break;
                case "down":
                    pos[1] = pos[1] - Body.getSIZE();
                    break;
            }
        }
        return pos;
    }

    /**
     * another massive pile of shit if our head touches the PowerUp, creates a
     * new body and lesser the WAIT_TIME
     */
    private void checkIfPowerUpped() {
        Body head = getBody(0);
        if ((head.getPosX() >= powerUp.getPosX() && head.getPosY() >= powerUp.getPosY()
                && head.getPosX() <= powerUp.getPosX() + Body.getSIZE() && head.getPosY() <= powerUp.getPosY() + Body.getSIZE())
                || (head.getPosX() + Body.getSIZE() >= powerUp.getPosX() && head.getPosY() + Body.getSIZE() >= powerUp.getPosY()
                && head.getPosX() + Body.getSIZE() <= powerUp.getPosX() + Body.getSIZE() && head.getPosY() + Body.getSIZE() <= powerUp.getPosY() + Body.getSIZE())) {
            powerUp = new Others("powerup");
            score += 10;
            Body lastBody = getBody(snake.size() - 1);
            if (score % 3 == 0) {
                obstacles.add(new Others("obstacle", lastBody.getPosX(), lastBody.getPosY()));
            }
            int[] pos = computeNewBodyPos();
            lastDirection = snake.get(lastBody);
            snake.put(new Body(pos[0], pos[1], lastDirection), lastDirection);
            WAIT_TIME = WAIT_TIME - 50 > 10 ? WAIT_TIME - 50 : WAIT_TIME - 5;
            timeline.stop();
            timeline.getKeyFrames().setAll(new KeyFrame(Duration.millis(WAIT_TIME), ae -> timeLineAction()));
            timeline.play();
        }
    }

    private Body getBody(int index) {
        Set<Body> keys = snake.keySet();
        for (Body body : keys) {
            if (body.getOrder() == index) {
                return body;
            }
        }
        return null;
    }

    private boolean isInBody() {
        Body head = getBody(0);
        for (Body body : snake.keySet()) {
            if (body.getType().equals("body") && ((head.getPosX() >= body.getPosX() && head.getPosY() >= body.getPosY()
                    && head.getPosX() <= body.getPosX() + Body.getSIZE() && head.getPosY() <= body.getPosY() + Body.getSIZE())
                    && (head.getPosX() + Body.getSIZE() >= body.getPosX() && head.getPosY() + Body.getSIZE() >= body.getPosY()
                    && head.getPosX() + Body.getSIZE() <= body.getPosX() + Body.getSIZE() && head.getPosY() + Body.getSIZE() <= body.getPosY() + Body.getSIZE()))) {
                return true;
            }
        }
        return false;
    }

    private boolean isInObstacle() {
        Body head = getBody(0);
        for (Others obstacle : obstacles) {
            if (obstacle.getType().equals("obstacle") && ((head.getPosX() >= obstacle.getPosX() && head.getPosY() >= obstacle.getPosY()
                    && head.getPosX() <= obstacle.getPosX() + Body.getSIZE() && head.getPosY() <= obstacle.getPosY() + Body.getSIZE())
                    || (head.getPosX() + Body.getSIZE() >= obstacle.getPosX() && head.getPosY() + Body.getSIZE() >= obstacle.getPosY()
                    && head.getPosX() + Body.getSIZE() <= obstacle.getPosX() + Body.getSIZE() && head.getPosY() + Body.getSIZE() <= obstacle.getPosY() + Body.getSIZE()))) {
                return true;
            }
        }
        return false;
    }

    private void checkIfFailed() {
        Body head = getBody(0);
        if (((head.getPosX() < 0 || head.getPosX() >= WIDTH)
                || head.getPosY() < 0 || head.getPosY() >= HEIGHT) || isInBody() || isInObstacle()) {
            timeline.stop();
            Platform.runLater(() -> {
                initScore();
                if (!STOP) {
                    primaryStage.setScene(scoreScene);
                    STOP = true;
                }
            });
        }
    }

    private void initScore() {
        BorderPane borderPane = new BorderPane();
        borderPane.getStyleClass().add("BlackBG");
        Label scoreLabel = new Label("Score: " + score);
        if (score > 90) {
            scoreLabel.setText(scoreLabel.getText() + "\nYou are now better than Guillaume");
        } else if (score > 150) {
            scoreLabel.setText(scoreLabel.getText() + "\nYou are now better than Kevin\n(who is a big pile of shit)");
        }
        borderPane.setCenter(scoreLabel);
        Button exit = new Button("Exit");
        exit.setOnAction((ActionEvent event) -> {
            Platform.exit();
        });
        Button reset = new Button("Reset");
        reset.setOnAction((ActionEvent event) -> {
            Body.setI(0);
            STOP = false;
            primaryStage.setScene(scene);
            initSnake();
        });
        borderPane.setBottom(new HBox(exit, reset));
        scoreScene = new Scene(borderPane, WIDTH, HEIGHT);

        scoreScene.getStylesheets()
                .add("snake/snake.css");
    }

    private void createObstacles() {
        obstacles = new ArrayList<>();
//        for (int i = 0; i <= 5; ++i) {
//            obstacles.add(new Others("obstacle"));
//        }
    }

    /**
     * Main function, creates the 2 first Body then call another Thread (Task)
     * to run an infinite loop in which we update everything every "WAIT_TIME"
     */
    private void initSnake() {
        snake = new HashMap<>();
        WAIT_TIME = INITIAL_WAIT_TIME;
        snake.put(new Body(WIDTH / 2, HEIGHT / 2, "right", Color.BLANCHEDALMOND, "head"), "right");
        snake.put(new Body(WIDTH / 2 - Body.getSIZE(), HEIGHT / 2, "right", Color.WHITE, "neck"), "right");
        createObstacles();
        nextDirection = "right";
        powerUp = new Others("powerup");
        score = 0;
        initHandlers();
        displaySnake();
        timeline = new Timeline(new KeyFrame(
                Duration.millis(WAIT_TIME),
                ae -> timeLineAction()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void timeLineAction() {
        //    instru.noteOn(60);
        computeSnakeDirection();
        checkIfFailed();
        checkIfPowerUpped();
        Platform.runLater(() -> {
            displaySnake();
        });
        canUpdate = true;
    }

    /**
     * stuff for Keyboard handling
     */
    private void initHandlers() {
        root.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if ((event.getCode() == KeyCode.D && !nextDirection.equals("left"))
                        || (event.getCode() == KeyCode.Q && !nextDirection.equals("right"))
                        || (event.getCode() == KeyCode.Z && !nextDirection.equals("down"))
                        || (event.getCode() == KeyCode.S && !nextDirection.equals("up"))) {
                    switch (event.getCode()) {
                        case D:
                            nextDirection = "right";
                            break;
                        case Q:
                            nextDirection = "left";
                            break;
                        case Z:
                            nextDirection = "up";
                            break;
                        case S:
                            nextDirection = "down";
                            break;
                    }
                    canUpdate = false;
                } else {
                    switch (event.getCode()) {
                        case RIGHT:
                            camera.getTransforms().add(new Rotate(TRANSFORM, Rotate.Y_AXIS));
                            break;
                        case LEFT:
                            camera.getTransforms().add(new Rotate(-TRANSFORM, Rotate.Y_AXIS));
                            break;
                        case UP:
                            camera.getTransforms().add(new Rotate(TRANSFORM, Rotate.X_AXIS));
                            break;
                        case DOWN:
                            camera.getTransforms().add(new Rotate(-TRANSFORM, Rotate.X_AXIS));
                            break;
                        case A:
                            camera.getTransforms().add(new Translate(0, 0, -TRANSFORM * 2));
                            break;
                        case E:
                            camera.getTransforms().add(new Translate(0, 0, TRANSFORM * 2));
                            break;
                    }
                }
            }
        });
    }

    /**
     * useless
     *
     * @param snakeFrame
     */
    private void initSnakeFrame(Canvas snakeFrame) {
        snakeGC = snakeFrame.getGraphicsContext2D();
    }

    /**
     * first function to be called, inits the frame and the stage
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        root = new Group();
        snakeFrame = new Canvas();
        camera = new PerspectiveCamera(false);
        camera.getTransforms().addAll(
                new Rotate(40, Rotate.X_AXIS),
                new Translate(0, -15, -110));
        root.getChildren().add(snakeFrame);
        root.getChildren().add(camera);
        snakeFrame.setWidth(WIDTH);
        snakeFrame.setHeight(HEIGHT);
        scene = new Scene(root, WIDTH, HEIGHT, false, SceneAntialiasing.BALANCED);
        scene.setCamera(camera);
        initSnakeFrame(snakeFrame);
        root.requestFocus();
        primaryStage.setTitle("Snake");
        primaryStage.setScene(scene);
        primaryStage.show();
        this.primaryStage = primaryStage;
        initSnake();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
