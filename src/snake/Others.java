package snake;

import java.util.Random;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;

/**
 *
 * @author da-sil_l
 */
public class Others extends Box {

    private int posX;
    private int posY;
    private final String type;
    private Color color;

    public Others(String type) {
        super(Body.getSIZE(), Body.getSIZE(), Body.getSIZE());
        this.type = type;
        switchColor();
        Random random = new Random();
        posX = random.nextInt(Snake.WIDTH);
        posY = random.nextInt(Snake.HEIGHT);
        posX = posX / 10;
        posX = posX * 10;
        posY = posY / 10;
        posY = posY * 10;
        initBox(posX, posY);
    }

    public Others(String type, int posX, int posY) {
        super(Body.getSIZE(), Body.getSIZE(), Body.getSIZE());
        this.type = type;
        switchColor();
        this.posX = posX;
        this.posY = posY;
        initBox(posX, posY);
    }

    private void initBox(int posX, int posY) {
        this.setLayoutX(posX);
        this.setLayoutY(posY);
        this.setMaterial(new PhongMaterial(color));
    }

    private void switchColor() {
        switch (type) {
            case "powerup":
                color = Color.GREEN;
                break;
            case "obstacle":
                color = Color.GREY;
                break;
        }
    }

    public Color getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

}
