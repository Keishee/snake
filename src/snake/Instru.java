/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

/**
 *
 * @author da-sil_l
 */
public class Instru {

    private Synthesizer synthetiseur;
    private MidiChannel canal;
    public int volume;

    public Instru() {

        volume = 100;
        try {
            synthetiseur = MidiSystem.getSynthesizer();
            synthetiseur.open();
        } catch (MidiUnavailableException ex) {
            Logger.getLogger(Instru.class.getName()).log(Level.SEVERE, null, ex);
        }
        canal = synthetiseur.getChannels()[0];

        canal.programChange(0);
    }

    public void noteOn(int note) {
        canal.noteOn(note, volume);
    }

    public void noteOff(int note) {
        canal.noteOff(note);
    }

    public void set_instrument(int instru) {
        canal.programChange(instru);
    }
}
