package snake;

import javafx.scene.effect.Bloom;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.DrawMode;

/**
 *
 * @author da-sil_l
 */
public class Body extends Box {

    private int posX;
    private int posY;
    private static final int SIZE = 10;
    private static final int ADVANCE = 10;
    private String direction;
    private Color color;
    private String type;
    private int order;
    private static int i = 0;

    /**
     * constructor, creates the Body
     *
     * @param posX
     * @param posY
     * @param direction
     */
    public Body(int posX, int posY, String direction) {
        this(posX, posY, direction, Color.WHITE, "body");
    }

    public Body(int posX, int posY, String direction, Color color, String type) {
        super(SIZE, SIZE, SIZE);
        this.order = i;
        this.posX = posX;
        this.posY = posY;
        this.direction = direction;
        this.color = color;
        this.type = type;
        this.setLayoutX(posX);
        this.setLayoutY(posY);
        this.setDrawMode(DrawMode.LINE);
        ++i;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public static int getSIZE() {
        return SIZE;
    }

    public String getDirection() {
        return direction;
    }

    public Color getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public int getOrder() {
        return order;
    }

    public static void setI(int i) {
        Body.i = i;
    }

    /**
     * when a direction is set, posX and posY updates accordingly
     *
     * @param direction
     */
    public void setDirection(String direction) {
        this.direction = direction;
        switch (direction) {
            case "right":
                posX = posX + ADVANCE;
                break;
            case "left":
                posX = posX - ADVANCE;
                break;
            case "up":
                posY = posY - ADVANCE;
                break;
            case "down":
                posY = posY + ADVANCE;
                break;
        }
        this.setLayoutX(posX);
        this.setLayoutY(posY);
    }

}
